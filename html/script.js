//http://stackoverflow.com/a/8260383
function youtube_parser(url){
    var regExp = /^.*((youtu.be\/)|(v\/)|(\/u\/\w\/)|(embed\/)|(watch\?))\??v?=?([^#\&\?]*).*/;
    var match = url.match(regExp);
    if (match&&match[7].length==11){
        return match[7];
    }else{
        console.error("Fail to parse URL")
    }
}

function updateEmbedSrc() {
    var input = document.getElementById("url-input").value;
    var iframe = document.getElementById("video-iframe");
    iframe.src = "http://www.youtube.com/embed/" + youtube_parser(input);
    iframe.contentWindow.location.reload(true);
}
